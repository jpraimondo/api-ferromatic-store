<?php
require_once 'clases/respuestas.class.php';
require_once 'clases/categoria.class.php';

$_respuestas = new respuestas;
$_categoria = new categoria;

// Hay que modificar esto no esta todo implementado //


if($_SERVER['REQUEST_METHOD'] == "GET"){

    if(isset($_GET['id'])){
        $categoriaid = $_GET['id'];
        $datosCategoria = $_categoria->obtenerCategoria($categoriaid);
        header("Content-Type: application/json");
        echo json_encode($datosCategoria);
        http_response_code(200);
    }else{
        $listaCategorias = $_categoria->listarCategorias();
        header("Content-Type: application/json");
        echo json_encode($listaCategorias);
        http_response_code(200);
    }
    
}
else if($_SERVER['REQUEST_METHOD'] == "POST"){
    //recibimos los datos enviados
    $postBody = file_get_contents("php://input");
    //enviamos los datos al manejador
    $datosArray = $_categoria->post($postBody);
    //delvovemos una respuesta 
     header('Content-Type: application/json');
     if(isset($datosArray["result"]["error_id"])){
         $responseCode = $datosArray["result"]["error_id"];
         http_response_code($responseCode);
     }else{
         http_response_code(200);
     }
     echo json_encode($datosArray);
    
}
else if($_SERVER['REQUEST_METHOD'] == "PUT"){
      //recibimos los datos enviados
      $postBody = file_get_contents("php://input");
      //enviamos datos al manejador
      $datosArray = $_categoria->put($postBody);
        //delvovemos una respuesta 
     header('Content-Type: application/json');
     if(isset($datosArray["result"]["error_id"])){
         $responseCode = $datosArray["result"]["error_id"];
         http_response_code($responseCode);
     }else{
         http_response_code(200);
     }
     echo json_encode($datosArray);

}else if($_SERVER['REQUEST_METHOD'] == "DELETE"){

        $headers = getallheaders();
        if(isset($headers["token"]) && isset($headers["categoriaid"])){
            //recibimos los datos enviados por el header
            $send = [
                "token" => $headers["token"],
                "categoriaid" =>$headers["categoriaid"]
            ];
            $postBody = json_encode($send);
        }else{
            //recibimos los datos enviados
            $postBody = file_get_contents("php://input");
        }
        
        //enviamos datos al manejador
        $datosArray = $_categoria->delete($postBody);
        //delvovemos una respuesta 
        header('Content-Type: application/json');
        if(isset($datosArray["result"]["error_id"])){
            $responseCode = $datosArray["result"]["error_id"];
            http_response_code($responseCode);
        }else{
            http_response_code(200);
        }
        echo json_encode($datosArray);
}
else{
    header('Content-Type: application/json');
    $datosArray = $_respuestas->error_405();
    echo json_encode($datosArray);
}


?>