<?php
require_once 'conexion/conexion.php';
require_once 'respuestas.class.php';


class auth extends conexion{

    public function login($json){
      
        $_respustas = new respuestas;
        $datos = json_decode($json,true);
        if(!isset($datos['correo']) || !isset($datos["password"])){
            //error con los campos
            return $_respustas->error_400();
        }else{
            //todo esta bien 
            $correo = $datos['correo'];
            $password = $datos['password'];
            
            $datos = $this->obtenerDatosUsuario($correo);
            if($datos){
                //verificar si la contraseña es igual
                    if(parent::decrypt($password,$datos[0]['password'])){
                            if($datos[0]['estado'] == "Activo"){
                                //crear el token
                                $verificar  = $this->insertarToken($datos[0]['usuarioid']);
                                if($verificar){
                                        // si se guardo
                                        $result = $_respustas->response;
                                        $result["result"] = array(
                                            "token" => $verificar
                                        );
                                        return $result;
                                }else{
                                        //error al guardar
                                        return $_respustas->error_500("Error interno, No hemos podido guardar");
                                }
                            }else{
                                //el usuario esta inactivo
                                return $_respustas->error_200("El usuario esta inactivo");
                            }
                    }else{
                        //la contraseña no es igual
                        return $_respustas->error_200("El password es invalido");
                    }
            }else{
                //no existe el usuario
                return $_respustas->error_200("El usuaro $correo  no existe ");
            }
        }
    }



    private function obtenerDatosUsuario($correo){
        $query = "SELECT usuarioid,password,estado FROM usuarios WHERE correo = '$correo'";
        $datos = parent::obtenerDatos($query);
        if(isset($datos[0]["usuarioid"])){
            return $datos;
        }else{
            return 0;
        }
    }


    private function insertarToken($usuarioid){
        $val = true;
        $token = bin2hex(openssl_random_pseudo_bytes(32,$val));
        $date = date("Y-m-d H:i");
        $estado = "Activo";
        $query = "INSERT INTO usuarios_token (UsuarioId,Token,Estado,Fecha)VALUES('$usuarioid','$token','$estado','$date')";
        $verifica = parent::nonQuery($query);
        if($verifica){
            return $token;
        }else{
            return 0;
        }
    }

    public function buscarTokenNuevo($token){
        $query = "SELECT  tokenid, usuarioid, estado from usuarios_token WHERE Token = '" . $token . "' AND Estado = 'Activo'";
        $resp = parent::obtenerDatos($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }


}

?>