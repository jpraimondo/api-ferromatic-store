<?php
require_once "conexion/conexion.php";
require_once "respuestas.class.php";

class categoria extends conexion{


    private $categoria = "categorias";
    private $categoriaid = "";
    private $nombre = "";
    private $descripcion ="";
    private $urlfoto ="";
    private $categoriapadre="";
    


    public function listarCategorias(){
        
        $query = "SELECT * FROM " . $this->categoria . " WHERE categoriapadre = '0'";
        $datos = parent::obtenerDatos($query);
        $i = 0;
        foreach($datos as $category){

           // $subcat = listaSubCategoria($category['categoriaid']);
            
            $subcategory["subcategoria"] = $this->listaSubCategoria($category['categoriaid']);
            
            $datos[$i]["subcategoria"] = $subcategory;
            $i++;
        }
        return ($datos);
    }

    public function obtenerCategoria($id){

        $query = "SELECT * FROM " . $this->categoria . " WHERE categoriaid = '$id'";
        return parent::obtenerDatos($query);

    }

    public function listaSubCategoria($id){

        $query = "SELECT * FROM " . $this->categoria . " WHERE categoriapadre = '$id'";
        return parent::obtenerDatos($query);

    }

    public function obtenerSubCategoria($id){

        $query = "SELECT * FROM " . $this->tablaHija . " WHERE subcategoriaid = '$id'";
        return parent::obtenerDatos($query);

    }



    public function post($json){
        $_respuestas = new respuestas;
        $datos = json_decode($json,true);

        if(!isset($datos['token'])){
                return $_respuestas->error_401();
        }else{
            $this->token = $datos['token'];
            $arrayToken = parent::buscarToken($datos['token']);
            if($arrayToken){

                if(!isset($datos['nombre']) || !isset($datos['categoriapadre']) || !isset($datos['urlfoto']) || !isset($datos['categoriapadre'])){
                    return $_respuestas->error_400();
                }else{
                    $this->nombre = $datos['nombre'];
                    $this->descripcion = $datos['descripcion'];
                    $this->urlfoto = $datos['urlfoto'];
                    $this->categoriapadre = $datos['categoriapadre'];
                    $resp = $this->insertarCategoria();
                    if($resp){
                        $respuesta = $_respuestas->response;
                        $respuesta["result"] = array(
                            "categoriaid" => $resp
                        );
                        return $respuesta;
                    }else{
                        return $_respuestas->error_500();
                    }
                }

            }else{
                return $_respuestas->error_401("El Token que envio es invalido o ha caducado");
            }
        }


       

    }


    private function insertarCategoria(){
        $query = "INSERT INTO ". $this->categoria . "( `nombre`, `descripcion`, `urlfoto`, `categoriapadre`) VALUES ('" . $this->nombre . "','" . $this->descripcion . "','" . $this->urlfoto . "','" . $this->categoriapadre . "')"; 
        $resp = parent::nonQueryId($query);
        if($resp){
             return $resp;
        }else{
            return 0;
        }
    }



}
?>