<?php


require_once "conexion/conexion.php";
require_once "respuestas.class.php";
require_once "utils.class.php";


class home extends conexion {



    private $tableProductos = "productos";
    private $tableDestacados = "productosdestacados";
    private $tableVendidos = "productosvendidos";
    private $tableBanners = "banners";
    private $tablePublicidad = "publicidad";
    private $tableRedesSociales = "redessociales";
    
    public function listaBanners(){
        
        $query = "SELECT * FROM " . $this->tableBanners . " ORDER BY orden ASC";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaPublicidad(){
        
        $query = "SELECT * FROM " . $this->tablePublicidad . " ORDER BY posicion ASC, orden ASC";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaProductosDestacados(){
        
        $query = "SELECT T2.*, T1.orden FROM " . $this->tableDestacados ." AS T1 
                INNER JOIN ". $this->tableProductos ." AS T2 ON T1.productoid = T2.productoid
                ORDER BY T1.orden ASC";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaProductosMasVendidos(){
        
        $query = "SELECT T2.*, T1.orden FROM " . $this->tableVendidos." AS T1 
                INNER JOIN ". $this->tableProductos ." AS T2 ON T1.productoid = T2.productoid
                ORDER BY T1.orden ASC";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaProductosNuevos(){
        
        $query = "SELECT * FROM " . $this->tableProductos . " ORDER BY fechacreacion DESC LIMIT 0,5";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaRedesSociales(){
        
        $query = "SELECT * FROM " . $this->tableRedesSociales . " LIMIT 1";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

/*
    public function listaTienda($pagina = 1){
        $inicio  = 0 ;
        $cantidad = 20;
        if($pagina > 1){
            $inicio = ($cantidad * ($pagina - 1)) +1 ;
            $cantidad = $cantidad * $pagina;
        }
        $query = "SELECT * FROM " . $this->table . " limit $inicio,$cantidad";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaTodasTiendas(){
        
        $query = "SELECT * FROM " . $this->table;
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function obtenerTiendaId($id){
        $query = "SELECT * FROM " . $this->table . " WHERE tiendaid = '$id'";
        return parent::obtenerDatos($query);

    }

    public function obtenerTiendaLocalidad($id){
        $query = "SELECT * FROM " . $this->table . " WHERE localidadid = '$id'";
        return parent::obtenerDatos($query);

    }
    public function obtenerTiendaCategoria($id){
        $query = "SELECT * FROM " . $this->table . " WHERE categoriaid = '$id'";
        return parent::obtenerDatos($query);

    }

    public function obtenerTiendaSlug($slug){
        $query = "SELECT * FROM " . $this->table . " WHERE slug = '$slug'";
        return parent::obtenerDatos($query);

    }

    public function obtenerFotosTienda($id){
        $query = "SELECT * FROM tiendafotos WHERE tiendaid = '$id'";
        return parent::obtenerDatos($query);
    }


    public function post($json){
        $_respuestas = new respuestas;
        $_utils = new utils;

        $datos = json_decode($json,true);

        if(!isset($datos['token'])){
                return $_respuestas->error_401();
        }else{
            $this->token = $datos['token'];
            $arrayToken =   parent::buscarToken($this->token);
            if($arrayToken){

                if(!isset($datos['nombre']) || !isset($datos['descripcion']))
                {
                    return $_respuestas->error_400();
                }else{
                    $this->nombre = $datos['nombre'];
                    $this->descripcion = $datos['descripcion'];
                    if(isset($datos['urlfoto'])) { $this->urlFoto = $datos['urlfoto']; }
                    if(isset($datos['categoriaid'])) { $this->categoriaId = $datos['categoriaid']; }
                    if(isset($datos['localidadid'])) { $this->localidadId = $datos['localidadid']; }
                    $this->slug = $_utils->createSlug($datos['nombre']);
                    $this->fechacreacion =  date('y-m-d');
                    $this->fechaactualizado =  date('y-m-d');
                    $this->estado = "Activo";
                    
                    $resp = $this->insertarTienda();
                    if($resp){
                        $respuesta = $_respuestas->response;
                        $respuesta["result"] = array(
                            "tiendaid" => $resp
                        );
                        return $respuesta;
                    }else{
                        return $_respuestas->error_500();
                    }
                }

            }else{
                return $_respuestas->error_401("El Token que envio es invalido o ha caducado");
            }
        }


       

    }

*/

}

?>