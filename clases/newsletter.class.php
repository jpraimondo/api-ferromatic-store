<?php

require_once "conexion/conexion.php";
require_once "respuestas.class.php";
require_once "utils.class.php";


class newsletter extends conexion {


    private $table = "newsletters";
    private $token = "";
    private $newsletterId = 0;
    private $correo = "";
    private $estado = 1;


    public function listaNewsletter($pagina = 1){
        $inicio  = 0 ;
        $cantidad = 20;
        if($pagina > 1){
            $inicio = ($cantidad * ($pagina - 1)) +1 ;
            $cantidad = $cantidad * $pagina;
        }
        $query = "SELECT * FROM " . $this->table . " limit $inicio,$cantidad";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaTodosNewsletter(){
        
        $query = "SELECT * FROM " . $this->table . " WHERE estado=1";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }


    public function post($json){
        $_respuestas = new respuestas;
        $_utils = new utils;

        $datos = json_decode($json,true);


        if(!isset($datos['correo']))
        {
            return $_respuestas->error_400();
        }
        else{
            $this->correo = $datos['correo'];
            $this->estado = 1;
                
            $resp = $this->insertarNewsletter();
            if($resp){
                $respuesta = $_respuestas->response;
                $respuesta["result"] = array("newsletterId" => $resp);
                    return $respuesta;
            }else{
                return $_respuestas->error_500();
            }
        }
}

    private function insertarNewsletter(){
        $query = "INSERT INTO `newsletters`(`correo`, `estado`) 
        VALUES ('" . $this->correo . "','" . $this->estado ."')"; 
        $resp = parent::nonQueryId($query);
        if($resp){
             return $resp;
        }else{
            return 0;
        }
    }

    
    public function put($json){
        $_respuestas = new respuestas;
        $_utils = new utils;

        $datos = json_decode($json,true);

            $this->newsletterId = $datos['newsletterid'];
            $this->correo = $datos['correo'];
            $this->estado = $datos['estado'];

            $resp = $this->modificarProducto();
            if($resp){
                $respuesta = $_respuestas->response;
                $respuesta["result"] = array(
                    "nesletterId" => $this->newsletterId
                );
                return $respuesta;
            }else{
                return $_respuestas->error_500();
            }
        }

    private function modificarProducto(){
        $query = "UPDATE " . $this->table . " SET correo ='" . $this->correo . "',estado = '" . $this->estado .
        "' WHERE newsletterid = '" . $this->newsletterId . "'"; 
        $resp = parent::nonQuery($query);
        if($resp >= 1){
             return $resp;
        }else{
            return 0;
        }
    }


}

?>