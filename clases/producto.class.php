<?php

require_once "conexion/conexion.php";
require_once "respuestas.class.php";
require_once "utils.class.php";


class producto extends conexion {


    private $table = "productos";
    private $productoid = "";
    private $nombre = "";
    private $descripcion = "";
    private $fotoPrincipal = "";
    private $slug = "";
    private $stock = 0;
    private $fechaCreacion = "";
    private $fechaActualizado = "";
    private $categoriaId = 0;
    private $categoriaNombre ="";
    private $tipoPrecioId = 0;
    private $tipoImpuestoId = 0;
    private $precio = 0;
    private $descuento = 0;
    private $estado = 0;
    private $token = "";


    public function listaProducto($pagina = 1){
        $inicio  = 0 ;
        $cantidad = 20;
        if($pagina > 1){
            $inicio = ($cantidad * ($pagina - 1)) +1 ;
            $cantidad = $cantidad * $pagina;
        }
        $query = "SELECT * FROM " . $this->table . " limit $inicio,$cantidad";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaTodosProducto(){
        
        $query = "SELECT * FROM " . $this->table;
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }


    public function obtenerProductoCategoria($id){
        $query = "SELECT * FROM " . $this->table . " WHERE categoriaid = '$id'";
        return parent::obtenerDatos($query);

    }

    public function obtenerProductoSlug($slug){
        $query = "SELECT * FROM " . $this->table . " WHERE slug = '$slug'";
        return parent::obtenerDatos($query);

    }

    public function obtenerFotosProducto($id){
        $query = "SELECT * FROM fotos WHERE productoid = '$id'";
        return parent::obtenerDatos($query);
    }


    public function post($json){
        $_respuestas = new respuestas;
        $_utils = new utils;

        $datos = json_decode($json,true);

        if(!isset($datos['token'])){
                return $_respuestas->error_401();
        }else{
            $this->token = $datos['token'];
            $arrayToken =   parent::buscarToken($this->token);
            if($arrayToken){

                if(!isset($datos['nombre']))
                {
                    return $_respuestas->error_400();
                }else{
                    $this->nombre = $datos['nombre'];
                    if(isset($datos['descripcion'])) { $this->descripcion = $datos['descripcion'];}
                    if(isset($datos['fotoprincipal'])) { $this->fotoPrincipal = $datos['fotoprincipal']; }
                    if(isset($datos['categoriaid'])) { $this->categoriaId = $datos['categoriaid']; }
                    if(isset($datos['stock'])) { $this->stock = $datos['stock']; }
                    if(isset($datos['tipoprecioid'])) { $this->tipoPrecioId = $datos['tipoprecioid']; }
                    if(isset($datos['tipoimpuestoid'])) { $this->tipoImpuestoId = $datos['tipoimpuestoid']; }
                    if(isset($datos['precio'])) { $this->precio = $datos['precio']; }
                    if(isset($datos['descuento'])) { $this->descuento = $datos['descuento']; }
                    $this->slug = $_utils->createSlug($datos['nombre']);
                    $this->fechacreacion =  date('y-m-d H:i');
                    $this->fechaactualizado =  date('y-m-d H:i');
                    $this->estado = 1;
                    
                    $resp = $this->insertarProducto();
                    if($resp){
                        $respuesta = $_respuestas->response;
                        $respuesta["result"] = array(
                            "productoid" => $resp
                        );
                        return $respuesta;
                    }else{
                        return $_respuestas->error_500();
                    }
                }

            }else{
                return $_respuestas->error_401("El Token que envio es invalido o ha caducado");
            }
        }   

    }

    private function insertarProducto(){
        $query = "INSERT INTO `productos`(`nombre`, `descripcion`, `fotoprincipal`, `slug`, `stock`, `fechacreacion`, `fechaactualizado`, `categoriaid`, `tipoprecioid`, `tipoimpuestoid`, `precio`, `descuento`, `estado`) 
        VALUES ('" . $this->nombre . "','" . $this->descripcion ."','" . $this->fotoPrincipal . "','"  . $this->slug . "','" . $this->stock . "','" . $this->fechacreacion . "','" . $this->fechaactualizado . "','" . $this->categoriaId . "','" . $this->tipoPrecioId . "','" . $this->tipoImpuestoId . "','" . $this->precio . "','" . $this->descuento . "','" . $this->estado . "')"; 
        $resp = parent::nonQueryId($query);
        if($resp){
             return $resp;
        }else{
            return 0;
        }
    }

    
    public function put($json){
        $_respuestas = new respuestas;
        $_utils = new utils;

        $datos = json_decode($json,true);

        if(!isset($datos['token'])){
            return $_respuestas->error_401();
        }else{
            $this->token = $datos['token'];
            $arrayToken =   parent::buscarToken($this->token);
            if($arrayToken){
                if(!isset($datos['productoid'])){
                    return $_respuestas->error_400();
                }else{
                    $this->productoid = $datos['productoid'];
                    $this->nombre = $datos['nombre'];
                    if(isset($datos['descripcion'])) { $this->descripcion = $datos['descripcion'];}
                    if(isset($datos['fotoprincipal'])) { $this->fotoPrincipal = $datos['fotoprincipal']; }
                    if(isset($datos['categoriaid'])) { $this->categoriaId = $datos['categoriaid']; }
                    if(isset($datos['stock'])) { $this->stock = $datos['stock']; }
                    if(isset($datos['tipoprecioid'])) { $this->tipoPrecioId = $datos['tipoprecioid']; }
                    if(isset($datos['tipoimpuestoid'])) { $this->tipoImpuestoId = $datos['tipoimpuestoid']; }
                    if(isset($datos['precio'])) { $this->precio = $datos['precio']; }
                    if(isset($datos['descuento'])) { $this->descuento = $datos['descuento']; }
                    $this->slug = $_utils->createSlug($datos['nombre']);
                    $this->fechaactualizado =  date('y-m-d');
                    if(isset($datos['estado'])) { $this->estado= $datos['estado']; }
                    echo($this->slug);
        
                    $resp = $this->modificarProducto();
                    if($resp){
                        $respuesta = $_respuestas->response;
                        $respuesta["result"] = array(
                            "productoid" => $this->productoid
                        );
                        return $respuesta;
                    }else{
                        return $_respuestas->error_500();
                    }
                }

            }else{
                return $_respuestas->error_401("El Token que envio es invalido o ha caducado");
            }
        }


    }

    private function modificarProducto(){
        $query = "UPDATE " . $this->table . " SET nombre ='" . $this->nombre . "',descripcion = '" . $this->descripcion . "', fotoprincipal = '" . $this->fotoPrincipal . "', categoriaid = '" .
        $this->categoriaId . "', stock = '" . $this->stock . "', tipoprecioid = '" . $this->tipoPrecioId . "', tipoimpuestoid = '" . $this->tipoImpuestoId . "', precio = '" . $this->precio .
        "', descuento = '" . $this->descuento . "', slug = '" . $this->slug . "', fechaactualizado = '" . $this->fechaactualizado . "', estado = '" . $this->estado .
        "' WHERE productoid = '" . $this->productoid . "'"; 
        $resp = parent::nonQuery($query);
        if($resp >= 1){
             return $resp;
        }else{
            return 0;
        }
    }


    public function delete($json){
        $_respuestas = new respuestas;
        $_utils = new utils;
        $datos = json_decode($json,true);

        if(!isset($datos['token'])){
            return $_respuestas->error_401();
        }else{
            $this->token = $datos['token'];
            $arrayToken = $_utils->buscarToken();
            if($arrayToken){

                if(!isset($datos['productoid'])){
                    return $_respuestas->error_400();
                }else{
                    $this->productoid = $datos['productoid'];
                    $resp = $this->eliminarProducto();
                    if($resp){
                        $respuesta = $_respuestas->response;
                        $respuesta["result"] = array(
                            "productoid" => $this->productoid
                        );
                        return $respuesta;
                    }else{
                        return $_respuestas->error_500();
                    }
                }

            }else{
                return $_respuestas->error_401("El Token que envio es invalido o ha caducado");
            }
        }
     
    }


    private function eliminarProducto(){
        $query = "DELETE FROM " . $this->table . " WHERE productoid= '" . $this->productoid . "'";
        $resp = parent::nonQuery($query);
        if($resp >= 1 ){
            return $resp;
        }else{
            return 0;
        }
    }

}

?>