<?php


require_once "conexion/conexion.php";
require_once "respuestas.class.php";
require_once "utils.class.php";


class tienda extends conexion {



    private $table = "tienda";
    private $tiendaid = "";
    private $nombre = "";
    private $descripcion = "";
    private $urlFoto = "";
    private $slug = "";
    private $fechaCreacion = "";
    private $fechaActualizado = "";
    private $categoriaId = 0;
    private $localidadId = 0;
    private $categoriaNombre ="";
    private $localidadNombre ="";
    private $token = "";


    public function listaTienda($pagina = 1){
        $inicio  = 0 ;
        $cantidad = 20;
        if($pagina > 1){
            $inicio = ($cantidad * ($pagina - 1)) +1 ;
            $cantidad = $cantidad * $pagina;
        }
        $query = "SELECT * FROM " . $this->table . " limit $inicio,$cantidad";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function listaTodasTiendas(){
        
        $query = "SELECT * FROM " . $this->table;
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    public function obtenerTiendaId($id){
        $query = "SELECT * FROM " . $this->table . " WHERE tiendaid = '$id'";
        return parent::obtenerDatos($query);

    }

    public function obtenerTiendaLocalidad($id){
        $query = "SELECT * FROM " . $this->table . " WHERE localidadid = '$id'";
        return parent::obtenerDatos($query);

    }
    public function obtenerTiendaCategoria($id){
        $query = "SELECT * FROM " . $this->table . " WHERE categoriaid = '$id'";
        return parent::obtenerDatos($query);

    }

    public function obtenerTiendaSlug($slug){
        $query = "SELECT * FROM " . $this->table . " WHERE slug = '$slug'";
        return parent::obtenerDatos($query);

    }

    public function obtenerFotosTienda($id){
        $query = "SELECT * FROM tiendafotos WHERE tiendaid = '$id'";
        return parent::obtenerDatos($query);
    }


    public function post($json){
        $_respuestas = new respuestas;
        $_utils = new utils;

        $datos = json_decode($json,true);

        if(!isset($datos['token'])){
                return $_respuestas->error_401();
        }else{
            $this->token = $datos['token'];
            $arrayToken =   parent::buscarToken($this->token);
            if($arrayToken){

                if(!isset($datos['nombre']) || !isset($datos['descripcion']))
                {
                    return $_respuestas->error_400();
                }else{
                    $this->nombre = $datos['nombre'];
                    $this->descripcion = $datos['descripcion'];
                    if(isset($datos['urlfoto'])) { $this->urlFoto = $datos['urlfoto']; }
                    if(isset($datos['categoriaid'])) { $this->categoriaId = $datos['categoriaid']; }
                    if(isset($datos['localidadid'])) { $this->localidadId = $datos['localidadid']; }
                    $this->slug = $_utils->createSlug($datos['nombre']);
                    $this->fechacreacion =  date('y-m-d');
                    $this->fechaactualizado =  date('y-m-d');
                    $this->estado = "Activo";
                    
                    $resp = $this->insertarTienda();
                    if($resp){
                        $respuesta = $_respuestas->response;
                        $respuesta["result"] = array(
                            "tiendaid" => $resp
                        );
                        return $respuesta;
                    }else{
                        return $_respuestas->error_500();
                    }
                }

            }else{
                return $_respuestas->error_401("El Token que envio es invalido o ha caducado");
            }
        }


       

    }

      /*
 `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `urlfoto` varchar(255) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `fechacreacion` date DEFAULT NULL,
  `fechaactualizado` date DEFAULT NULL,
  `categoriaid` int(5) DEFAULT NULL,
  `localidadid` int(5) DEFAULT NULL,
  `estado` varchar(10) DEFAULT NULL
    */


    private function insertarTienda(){
        $query = "INSERT INTO " . $this->table . " (nombre,descripcion,urlfoto,slug,fechacreacion,fechaactualizado,categoriaid,localidadid,estado)
        values ('" . $this->nombre . "','" . $this->descripcion ."','" . $this->urlFoto . "','"  . $this->slug . "','" . $this->fechacreacion . "','" . $this->fechaactualizado . "','" . $this->categoriaId . "','" . $this->localidadId . "','" . $this->estado . "')"; 
        $resp = parent::nonQueryId($query);
        if($resp){
             return $resp;
        }else{
            return 0;
        }
    }


    /// Hasta aca esta implementado correctamente ///


    /*
    public function put($json){
        $_respuestas = new respuestas;
        $datos = json_decode($json,true);

        if(!isset($datos['token'])){
            return $_respuestas->error_401();
        }else{
            $this->token = $datos['token'];
            $arrayToken =   $this->buscarToken();
            if($arrayToken){
                if(!isset($datos['pacienteId'])){
                    return $_respuestas->error_400();
                }else{
                    $this->pacienteid = $datos['pacienteId'];
                    if(isset($datos['nombre'])) { $this->nombre = $datos['nombre']; }
                    if(isset($datos['dni'])) { $this->dni = $datos['dni']; }
                    if(isset($datos['correo'])) { $this->correo = $datos['correo']; }
                    if(isset($datos['telefono'])) { $this->telefono = $datos['telefono']; }
                    if(isset($datos['direccion'])) { $this->direccion = $datos['direccion']; }
                    if(isset($datos['codigoPostal'])) { $this->codigoPostal = $datos['codigoPostal']; }
                    if(isset($datos['genero'])) { $this->genero = $datos['genero']; }
                    if(isset($datos['fechaNacimiento'])) { $this->fechaNacimiento = $datos['fechaNacimiento']; }
        
                    $resp = $this->modificarPaciente();
                    if($resp){
                        $respuesta = $_respuestas->response;
                        $respuesta["result"] = array(
                            "pacienteId" => $this->pacienteid
                        );
                        return $respuesta;
                    }else{
                        return $_respuestas->error_500();
                    }
                }

            }else{
                return $_respuestas->error_401("El Token que envio es invalido o ha caducado");
            }
        }


    }


    private function modificarPaciente(){
        $query = "UPDATE " . $this->table . " SET Nombre ='" . $this->nombre . "',Direccion = '" . $this->direccion . "', DNI = '" . $this->dni . "', CodigoPostal = '" .
        $this->codigoPostal . "', Telefono = '" . $this->telefono . "', Genero = '" . $this->genero . "', FechaNacimiento = '" . $this->fechaNacimiento . "', Correo = '" . $this->correo .
         "' WHERE PacienteId = '" . $this->pacienteid . "'"; 
        $resp = parent::nonQuery($query);
        if($resp >= 1){
             return $resp;
        }else{
            return 0;
        }
    }


    public function delete($json){
        $_respuestas = new respuestas;
        $datos = json_decode($json,true);

        if(!isset($datos['token'])){
            return $_respuestas->error_401();
        }else{
            $this->token = $datos['token'];
            $arrayToken =   $this->buscarToken();
            if($arrayToken){

                if(!isset($datos['pacienteId'])){
                    return $_respuestas->error_400();
                }else{
                    $this->pacienteid = $datos['pacienteId'];
                    $resp = $this->eliminarPaciente();
                    if($resp){
                        $respuesta = $_respuestas->response;
                        $respuesta["result"] = array(
                            "pacienteId" => $this->pacienteid
                        );
                        return $respuesta;
                    }else{
                        return $_respuestas->error_500();
                    }
                }

            }else{
                return $_respuestas->error_401("El Token que envio es invalido o ha caducado");
            }
        }



     
    }


    private function eliminarPaciente(){
        $query = "DELETE FROM " . $this->table . " WHERE PacienteId= '" . $this->pacienteid . "'";
        $resp = parent::nonQuery($query);
        if($resp >= 1 ){
            return $resp;
        }else{
            return 0;
        }
    }
*/


/*
    private function buscarToken(){
        $query = "SELECT  tokenid,usuarioid,estado from usuarios_token WHERE Token = '" . $this->token . "' AND Estado = 'Activo'";
        $resp = parent::obtenerDatos($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }


    private function actualizarToken($tokenid){
        $date = date("Y-m-d H:i");
        $query = "UPDATE usuarios_token SET Fecha = '$date' WHERE TokenId = '$tokenid' ";
        $resp = parent::nonQuery($query);
        if($resp >= 1){
            return $resp;
        }else{
            return 0;
        }
    }
*/


}





?>