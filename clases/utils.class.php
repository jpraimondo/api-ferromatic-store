<?php


class utils{

public function encrypt($string){
    return password_hash($string, PASSWORD_DEFAULT);
}

public function decrypt($string,$hash){
    return password_verify($string, $hash);
}

//Funcion para crear Slug

public function createSlug($nombreTienda){
    $slug = preg_replace('/[^A-Za-z0-9-]+/','-',$nombreTienda);
    $slug = strtolower($slug);
    return $slug;
}

}

?>