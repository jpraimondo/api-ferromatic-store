-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-07-2021 a las 00:23:54
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbferromatic`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

CREATE TABLE `banners` (
  `bannerid` int(11) NOT NULL,
  `urlbanner` varchar(255) DEFAULT NULL,
  `descripcionbanner` varchar(255) DEFAULT NULL,
  `orden` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicidad`
--

CREATE TABLE `publicidad` (
  `publicidadid` int(11) NOT NULL,
  `urlpublicidad` varchar(255) DEFAULT NULL,
  `descripcionpublicidad` varchar(255) DEFAULT NULL,
  `posicion` int(2) DEFAULT NULL,
  `orden` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `categoriaid` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `urlfoto` varchar(255) DEFAULT NULL,
  `categoriapadre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`categoriaid`, `nombre`, `descripcion`, `urlfoto`, `categoriapadre`) VALUES
(1, 'categoria1', 'Esta es la categoria 1', 'url', 0),
(2, 'categoria2', 'Esta es la categoria 2', 'url', 0),
(3, 'categoria3', 'Esta es la categoria 3', 'url', 0),
(4, 'subcategoria1', 'Esta es la subcategoria 1', 'url', 1),
(5, 'subcategoria2', 'Esta es la subcategoria 1', 'url', 1),
(6, 'subcategoria3', 'Esta es la subcategoria 3', 'url', 2),
(7, 'subcategoria4', 'Esta es la subcategoria 4', 'url', 2),
(8, 'subcategoria5', 'Esta es la subcategoria 5', 'url', 2),
(9, 'subcategoria6', 'Esta es la subcategoria 6', 'url', 3),
(10, 'subcategoria7', 'Esta es la subcategoria 7', 'url', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotos`
--

CREATE TABLE `fotos` (
  `fotoid` int(11) NOT NULL,
  `productoid` int(11) NOT NULL,
  `urlfoto` varchar(255) DEFAULT NULL,
  `descripcionfoto` varchar(255) DEFAULT NULL,
  `orden` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newsletters`
--

CREATE TABLE `newsletters` (
  `newsletterid` int(11) NOT NULL,
  `correo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `productoid` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `fotoprincipal` varchar(255) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `stock` int(5) DEFAULT NULL,
  `fechacreacion` datetime DEFAULT NULL,
  `fechaactualizado` datetime DEFAULT NULL,
  `categoriaid` int(5) DEFAULT NULL,
  `tipoprecioid` int(5) DEFAULT NULL,
  `tipoimpuestoid` int(5) DEFAULT NULL,
  `precio` decimal(8,2) DEFAULT NULL,
  `descuento` int(3) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`productoid`, `nombre`, `descripcion`, `fotoprincipal`, `slug`, `stock`, `fechacreacion`, `fechaactualizado`, `categoriaid`, `tipoprecioid`, `tipoimpuestoid`, `precio`, `descuento`, `estado`) VALUES
(1, 'Salsa de tomate', 'Salsa de tomate heinz 14oz', 'https://d13lnhwm7sh4hi.cloudfront.net/wp-content/uploads/2019/10/29180944/10013000007280-heinz-salsa-tomate-ketchup-14oz-01.jpg', 'salsa-de-tomate', 100, '2021-04-28 00:00:00', '2021-04-28 00:00:00', 0, 1, 1, '88.00', 0, 1),
(2, 'Salsa de tomate', 'Salsa de tomate heinz 14oz', 'https://d13lnhwm7sh4hi.cloudfront.net/wp-content/uploads/2019/10/29180944/10013000007280-heinz-salsa-tomate-ketchup-14oz-01.jpg', 'salsa-de-tomate', 100, '2021-04-28 00:00:00', '2021-04-28 00:00:00', 0, 1, 1, '88.00', 0, 1),
(3, 'Salsa tipo Mayonesa', 'Salsa tipo mayonesa Mc. Cornick con jugo natural de limon', 'https://d1e3z2jco40k3v.cloudfront.net/-/media/mccormick-us/products/mccormick/m/800/mayonnaise-with-lime-juice.png?rev=3ad0858d9417488a899639440fba72fa&vd=20200628T211557Z&hash=0EBB9F388A6E18469366DD86777C0F1F', 'salsa-tipo-mayonesa', 100, '2021-04-28 00:00:00', '2021-04-28 00:00:00', 0, 1, 1, '88.00', 0, 1),
(4, 'Caja de Te La Virginia 10 sobres limon', 'Caja de Te La Virginia 10 sobres sabor limon', 'https://d391ci4kxgasl8.cloudfront.net/fit-in/524x480/filters:fill(FFFFFF):quality(90):format(webp)/_img_productos/te-la-virginia-10s-foto.jpg', 'caja-de-te-la-virginia-10-sobres-limon', 100, '2021-04-29 00:00:00', '2021-07-08 00:00:00', 2, 1, 1, '199.00', 0, 0),
(5, 'Caja de Te La Virginia 100 sobres', 'Caja de Te La Virginia 100 sobres sabor limon', 'https://d391ci4kxgasl8.cloudfront.net/fit-in/524x480/filters:fill(FFFFFF):quality(90):format(webp)/_img_productos/te-la-virginia-10s-foto.jpg', 'caja-de-te-la-virginia-100-sobres', 100, '2021-05-17 00:00:00', '2021-05-17 00:00:00', 2, 1, 1, '88.00', 0, 1),
(6, 'Tarro de cafe 80Grs Bracafe', 'Tarro de cafe 80Grs Bracafe, el cafe de la familia, material vidrio', 'https://d391ci4kxgasl8.cloudfront.net/fit-in/524x480/filters:fill(FFFFFF):quality(90):format(webp)/_img_productos/te-la-virginia-10s-foto.jpg', 'tarro-de-cafe-80grs-bracafe', 100, '2021-06-11 00:00:00', '2021-06-11 00:00:00', 2, 1, 1, '88.00', 0, 1),
(7, 'Tarro de cafe 180Grs Bracafe', 'Tarro de cafe 180Grs Bracafe, el cafe de la familia, material vidrio', 'https://d391ci4kxgasl8.cloudfront.net/fit-in/524x480/filters:fill(FFFFFF):quality(90):format(webp)/_img_productos/te-la-virginia-10s-foto.jpg', 'tarro-de-cafe-180grs-bracafe', 100, '2021-06-11 01:56:00', '2021-06-11 01:56:00', 2, 1, 1, '88.00', 0, 1),
(8, 'Tarro de cafe 220Grs Bracafe', 'Tarro de cafe 220Grs Bracafe, el cafe de la familia, material vidrio', 'https://d391ci4kxgasl8.cloudfront.net/fit-in/524x480/filters:fill(FFFFFF):quality(90):format(webp)/_img_productos/te-la-virginia-10s-foto.jpg', 'tarro-de-cafe-220grs-bracafe', 100, '2021-06-11 01:56:00', '2021-06-11 01:56:00', 2, 1, 1, '88.00', 0, 1),
(9, 'Tarro de cafe 400Grs Bracafe', 'Tarro de cafe 400Grs Bracafe, el cafe de la familia, material vidrio', 'https://d391ci4kxgasl8.cloudfront.net/fit-in/524x480/filters:fill(FFFFFF):quality(90):format(webp)/_img_productos/te-la-virginia-10s-foto.jpg', 'tarro-de-cafe-400grs-bracafe', 100, '2021-06-11 02:01:00', '2021-06-11 02:01:00', 2, 1, 1, '88.00', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosdestacados`
--

CREATE TABLE `productosdestacados` (
  `productodestacadosid` int(5) NOT NULL,
  `productoid` int(11) NOT NULL,
  `orden` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosnuevos`
--

CREATE TABLE `productosnuevos` (
  `productonuevoid` int(5) NOT NULL,
  `productoid` int(11) NOT NULL,
  `orden` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosvendidos`
--

CREATE TABLE `productosvendidos` (
  `productovendidosid` int(5) NOT NULL,
  `productoid` int(11) NOT NULL,
  `orden` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redessociales`
--

CREATE TABLE `redessociales` (
  `instagram` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(255) DEFAULT NULL,
  `tiktok` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuarioid` int(11) NOT NULL,
  `correo` varchar(60) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fechacreacion` date DEFAULT NULL,
  `fechaactualizado` date DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuarioid`, `correo`, `nombre`, `direccion`, `telefono`, `password`, `fechacreacion`, `fechaactualizado`, `estado`) VALUES
(1, 'usuario1@gmail.com', NULL, NULL, NULL, '$2y$10$c1u5UZY7Wy5mkIow0sc.BuHVn4E7ZkPp355eQ69u3IND.c3I/kOKu', NULL, NULL, 'Activo'),
(2, 'usuario2@gmail.com', NULL, NULL, NULL, '$2y$10$c1u5UZY7Wy5mkIow0sc.BuHVn4E7ZkPp355eQ69u3IND.c3I/kOKu', NULL, NULL, 'Activo'),
(3, 'usuario3@gmail.com', NULL, NULL, NULL, '$2y$10$c1u5UZY7Wy5mkIow0sc.BuHVn4E7ZkPp355eQ69u3IND.c3I/kOKu', NULL, NULL, 'Activo'),
(4, 'usuario4@gmail.com', NULL, NULL, NULL, '$2y$10$c1u5UZY7Wy5mkIow0sc.BuHVn4E7ZkPp355eQ69u3IND.c3I/kOKu', NULL, NULL, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_token`
--

CREATE TABLE `usuarios_token` (
  `tokenid` int(11) NOT NULL,
  `usuarioid` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `estado` varchar(45) CHARACTER SET armscii8 DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios_token`
--

INSERT INTO `usuarios_token` (`tokenid`, `usuarioid`, `token`, `estado`, `fecha`) VALUES
(1, 1, 'da144ec5c0daee8d5705bc6c6d6fbb85739b7f506dba586d829f413288961cf8', 'Activo', '2021-04-28 19:09:00'),
(2, 1, '08121c76c83f4de7d28d31ace796f9ad8bbb99d9419f6b921a3bda5e184b6a66', 'Activo', '2021-04-28 19:09:00'),
(3, 1, 'd902fcd4eb4ea26fe48258ebd46813373a06003b7ba171d3e1e651f3cf61ab9f', 'Activo', '2021-05-17 00:19:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`bannerid`);


--
-- Indices de la tabla `publicidad`
--
ALTER TABLE `publicidad`
  ADD PRIMARY KEY (`publicidadid`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`categoriaid`);

--
-- Indices de la tabla `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`fotoid`);

--
-- Indices de la tabla `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`newsletterid`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`productoid`);

--
-- Indices de la tabla `productosdestacados`
--
ALTER TABLE `productosdestacados`
  ADD PRIMARY KEY (`productodestacadosid`);

--
-- Indices de la tabla `productosnuevos`
--
ALTER TABLE `productosnuevos`
  ADD PRIMARY KEY (`productonuevoid`);

--
-- Indices de la tabla `productosvendidos`
--
ALTER TABLE `productosvendidos`
  ADD PRIMARY KEY (`productovendidosid`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuarioid`);

--
-- Indices de la tabla `usuarios_token`
--
ALTER TABLE `usuarios_token`
  ADD PRIMARY KEY (`tokenid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banners`
--
ALTER TABLE `banners`
  MODIFY `bannerid` int(11) NOT NULL AUTO_INCREMENT;


--
-- AUTO_INCREMENT de la tabla `publicidad`
--
ALTER TABLE `publicidad`
  MODIFY `publicidadid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `categoriaid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `fotos`
--
ALTER TABLE `fotos`
  MODIFY `fotoid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `newsletterid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `productoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `productosdestacados`
--
ALTER TABLE `productosdestacados`
  MODIFY `productodestacadosid` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productosnuevos`
--
ALTER TABLE `productosnuevos`
  MODIFY `productonuevoid` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productosvendidos`
--
ALTER TABLE `productosvendidos`
  MODIFY `productovendidosid` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuarioid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios_token`
--
ALTER TABLE `usuarios_token`
  MODIFY `tokenid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
