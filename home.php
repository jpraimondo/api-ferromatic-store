<?php
require_once 'clases/respuestas.class.php';
require_once 'clases/home.class.php';

$_respuestas = new respuestas;
$_home = new home;

// Hay que modificar esto no esta todo implementado //


if($_SERVER['REQUEST_METHOD'] == "GET"){
  
    try{

    
        $datosHome = array();
        $datosBanner= $_home->listaBanners();
        $datosMasVendidos= $_home->listaProductosMasVendidos();
        $datosMasNuevos= $_home->listaProductosNuevos();
        $datosDestacados= $_home->listaProductosDestacados();
        $datosPublicidad= $_home->listaPublicidad();
        $datosRedesSociales= $_home->listaRedesSociales();
        $datosHome['banner'] = $datosBanner;
        $datosHome['masVendidos'] = $datosMasVendidos;
        $datosHome['masNuevos'] = $datosMasNuevos;
        $datosHome['publicidad'] = $datosPublicidad;
        $datosHome['masDestacados'] = $datosDestacados;
        $datosHome['redesSociales'] = $datosRedesSociales;
        header("Content-Type: application/json");
        echo json_encode($datosHome);
        http_response_code(200);

    }catch(Exception $e){
        header('Content-Type: application/json');
        $datosArray = $_respuestas->error_500($e->getMessage());
        echo json_encode($datosArray);
    }
}else{
    header('Content-Type: application/json');
    $datosArray = $_respuestas->error_405();
    echo json_encode($datosArray);
}


?>