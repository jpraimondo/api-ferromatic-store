<?php
require_once 'clases/respuestas.class.php';
require_once 'clases/newsletter.class.php';
require_once 'clases/auth.class.php';

$_respuestas = new respuestas;
$_newsletter = new newsletter;
$_auth = new auth;

// Hay que modificar esto no esta todo implementado //


if($_SERVER['REQUEST_METHOD'] == "GET"){

    $headers = getallheaders();

    if(!(isset($headers['Authorization']))){
        header('Content-Type: application/json');
        $datosArray = $_respuestas->error_401("Usuario no autorizado, por favor inicie sesion o consulte al administrador.");
        echo json_encode($datosArray);
        die();
    }else
    {
        $token = $headers['Authorization'];    
        $resp = $_auth->buscarTokenNuevo($token);
        if($resp == 0)
        {
            header('Content-Type: application/json');
            $datosArray = $_respuestas->error_401("Usuario no autorizado, por favor inicie sesion o consulte al administrador.");
            echo json_encode($datosArray);
            die();
        }    

    }

    if(isset($_GET["page"])){
    try{
        $pagina = $_GET["page"];
        $listaNewsletter = $_newsletter->listaNewsletter($pagina);
        header("Content-Type: application/json");
        echo json_encode($listaNewsletter);
        http_response_code(200);
    }catch(Exception $e){
            header('Content-Type: application/json');
            $datosArray = $_respuestas->error_500($e->getMessage());
            echo json_encode($datosArray);
        }

    }else{
        try{
        $listaNewsletter = $_newsletter->listaTodosNewsletter();
        header("Content-Type: application/json");
        echo json_encode($listaNewsletter);
        http_response_code(200);
    }catch(Exception $e){
        header('Content-Type: application/json');
        $datosArray = $_respuestas->error_500($e->getMessage());
        echo json_encode($datosArray);
    }
    }
    
}else if($_SERVER['REQUEST_METHOD'] == "POST"){
    //recibimos los datos enviados
    $postBody = file_get_contents("php://input");
    //enviamos los datos al manejador
    $datosArray = $_newsletter->post($postBody);
    //delvovemos una respuesta 
     header('Content-Type: application/json');
     if(isset($datosArray["result"]["error_id"])){
         $responseCode = $datosArray["result"]["error_id"];
         http_response_code($responseCode);
     }else{
         http_response_code(200);
     }
     echo json_encode($datosArray);
    
}else if($_SERVER['REQUEST_METHOD'] == "PUT")
{
    $headers = getallheaders();

    if(!(isset($headers['Authorization']))){
        header('Content-Type: application/json');
        $datosArray = $_respuestas->error_401("Usuario no autorizado, por favor inicie sesion o consulte al administrador.");
        echo json_encode($datosArray);
        die();
    }else
    {
        $token = $headers['Authorization'];    
        $resp = $_auth->buscarTokenNuevo($token);
        if($resp == 0)
        {
            header('Content-Type: application/json');
            $datosArray = $_respuestas->error_401("Usuario no autorizado, por favor inicie sesion o consulte al administrador.");
            echo json_encode($datosArray);
            die();
        }    

    }

    $postBody = file_get_contents("php://input");
    //enviamos los datos al manejador
    $datosArray = $_newsletter->put($postBody);
    //delvovemos una respuesta 
     header('Content-Type: application/json');
     if(isset($datosArray["result"]["error_id"])){
         $responseCode = $datosArray["result"]["error_id"];
         http_response_code($responseCode);
     }else{
         http_response_code(200);
     }
     echo json_encode($datosArray);

}
else{
    header('Content-Type: application/json');
    $datosArray = $_respuestas->error_405();
    echo json_encode($datosArray);
}


?>