<?php
require_once 'clases/respuestas.class.php';
require_once 'clases/tienda.class.php';

$_respuestas = new respuestas;
$_tienda = new tienda;

// Hay que modificar esto no esta todo implementado //


if($_SERVER['REQUEST_METHOD'] == "GET"){

    if(isset($_GET["page"])){
        $pagina = $_GET["page"];
        $listaTienda = $_tienda->listaTienda($pagina);
        header("Content-Type: application/json");
        echo json_encode($listaTienda);
        http_response_code(200);
    }else if(isset($_GET['id'])){
        $tiendaid = $_GET['id'];
        $datosTienda = $_tienda->obtenerTiendaId($tiendaid);
        $fotosTienda = $_tienda->obtenerFotosTienda($tiendaid);
        $datosTienda[0]['fotos'] = $fotosTienda;
        header("Content-Type: application/json");
        echo json_encode($datosTienda);
        http_response_code(200);
    }else if(isset($_GET['slug'])){
        $slug = $_GET['slug'];
        $datosTienda = $_tienda->obtenerTiendaSlug($slug);
        $fotosTienda = $_tienda->obtenerFotosTienda($datosTienda[0]['tiendaid']);
        $datosTienda[0]['fotos'] = $fotosTienda;
        header("Content-Type: application/json");
        echo json_encode($datosTienda);
        http_response_code(200);
    }else{
        $listaTiendas = $_tienda->listaTodasTiendas();
        header("Content-Type: application/json");
        echo json_encode($listaTiendas);
        http_response_code(200);
    }
    
}else if($_SERVER['REQUEST_METHOD'] == "POST"){
    //recibimos los datos enviados
    $postBody = file_get_contents("php://input");
    //enviamos los datos al manejador
    $datosArray = $_tienda->post($postBody);
    //delvovemos una respuesta 
     header('Content-Type: application/json');
     if(isset($datosArray["result"]["error_id"])){
         $responseCode = $datosArray["result"]["error_id"];
         http_response_code($responseCode);
     }else{
         http_response_code(200);
     }
     echo json_encode($datosArray);
    
}else if($_SERVER['REQUEST_METHOD'] == "PUT"){
      //recibimos los datos enviados
      $postBody = file_get_contents("php://input");
      //enviamos datos al manejador
      $datosArray = $_tienda->put($postBody);
        //delvovemos una respuesta 
     header('Content-Type: application/json');
     if(isset($datosArray["result"]["error_id"])){
         $responseCode = $datosArray["result"]["error_id"];
         http_response_code($responseCode);
     }else{
         http_response_code(200);
     }
     echo json_encode($datosArray);

}else if($_SERVER['REQUEST_METHOD'] == "DELETE"){

        $headers = getallheaders();
        if(isset($headers["token"]) && isset($headers["pacienteId"])){
            //recibimos los datos enviados por el header
            $send = [
                "token" => $headers["token"],
                "pacienteId" =>$headers["pacienteId"]
            ];
            $postBody = json_encode($send);
        }else{
            //recibimos los datos enviados
            $postBody = file_get_contents("php://input");
        }
        
        //enviamos datos al manejador
        $datosArray = $_tienda->delete($postBody);
        //delvovemos una respuesta 
        header('Content-Type: application/json');
        if(isset($datosArray["result"]["error_id"])){
            $responseCode = $datosArray["result"]["error_id"];
            http_response_code($responseCode);
        }else{
            http_response_code(200);
        }
        echo json_encode($datosArray);
       

}else{
    header('Content-Type: application/json');
    $datosArray = $_respuestas->error_405();
    echo json_encode($datosArray);
}


?>